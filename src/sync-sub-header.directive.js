module.directive("syncSubheader", function($templateCache) {
    return {
        restrict: "E",
        template: $templateCache.get('sync-subheader.tpl.html'),
        replace: true,
        scope:{
            loading : '=',
            errorList: '='
        },
        link: function(scope) {
            scope.showDetails = false;
            scope.visible = scope.errorList && scope.errorList.length > 0;

            scope.close = function(){
                scope.showDetails = false;
                scope.visible = false;
            };

            scope.toggleDetails = function(){
                scope.showDetails = !scope.showDetails;
            };


                scope.$watchCollection("errorList", function(){
               scope.visible = scope.errorList && scope.errorList.length > 0;
            });
        }
    }
});