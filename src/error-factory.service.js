module.factory("ErrorFactory", function(){

    var self = this;

    self.create = function(statusCode, message, method){
        var error = {
            statusCode: statusCode,
            message: message,
            method: method
        }

        return error;
    }

    return self;
});