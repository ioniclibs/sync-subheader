var module = angular.module('sync-subheader', ['pascalprecht.translate', 'ion-sync.templates']);

module.config(function($translateProvider){

    $translateProvider.useSanitizeValueStrategy(null);

    $translateProvider.translations('pt-BR', {
        'STATUS' : 'Status',
        'SYNC_ERROR' : 'Ocorreu um erro durante a sincronização.',
        'UNKNOWN_ERROR': 'Não foi possível obter detalhes do erro.'
    });

    // Define o idioma preferencial
    $translateProvider.preferredLanguage("pt-BR");

    // Define o idioma em caso de erro
    $translateProvider.fallbackLanguage("pt-BR");
});