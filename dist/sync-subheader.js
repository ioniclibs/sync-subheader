var module = angular.module('sync-subheader', ['pascalprecht.translate', 'ion-sync.templates']);

module.config(["$translateProvider", function($translateProvider){

    $translateProvider.useSanitizeValueStrategy(null);

    $translateProvider.translations('pt-BR', {
        'STATUS' : 'Status',
        'SYNC_ERROR' : 'Ocorreu um erro durante a sincronização.',
        'UNKNOWN_ERROR': 'Não foi possível obter detalhes do erro.'
    });

    // Define o idioma preferencial
    $translateProvider.preferredLanguage("pt-BR");

    // Define o idioma em caso de erro
    $translateProvider.fallbackLanguage("pt-BR");
}]);
angular.module("ion-sync.templates", []);
module.factory("ErrorFactory", function(){

    var self = this;

    self.create = function(statusCode, message, method){
        var error = {
            statusCode: statusCode,
            message: message,
            method: method
        }

        return error;
    }

    return self;
});
module.directive("syncSubheader", ["$templateCache", function($templateCache) {
    return {
        restrict: "E",
        template: $templateCache.get('sync-subheader.tpl.html'),
        replace: true,
        scope:{
            loading : '=',
            errorList: '='
        },
        link: function(scope) {
            scope.showDetails = false;
            scope.visible = scope.errorList && scope.errorList.length > 0;

            scope.close = function(){
                scope.showDetails = false;
                scope.visible = false;
            };

            scope.toggleDetails = function(){
                scope.showDetails = !scope.showDetails;
            };


                scope.$watchCollection("errorList", function(){
               scope.visible = scope.errorList && scope.errorList.length > 0;
            });
        }
    }
}]);
angular.module("ion-sync.templates").run(["$templateCache", function($templateCache) {$templateCache.put("sync-subheader.tpl.html","<div class=\"bar bar-subheader sync-subheader\">\r\n    <div class=\"loader-bar\" ng-show=\"loading\"></div>\r\n\r\n    <div class=\"list\" ng-show=\"visible\">\r\n\r\n        <a class=\"button button-icon icon ion-ios-close-empty\" ng-click=\"close()\"></a>\r\n\r\n        <div class=\"item item-text-wrap item-icon-left\" ng-click=\"toggleDetails()\">\r\n            <i class=\"icon\" ng-class=\"showDetails ? \'ion-ios-arrow-up\' : \'ion-ios-arrow-down\'\"></i>\r\n            <p>{{\'SYNC_ERROR\' | translate}}</p>\r\n        </div>\r\n\r\n        <div class=\"item item-text-wrap item-icon-left\" ng-repeat=\"error in errorList\" ng-show=\"showDetails\">\r\n            <i class=\"icon\" ng-class=\"error.method == \'put\'? \'ion-ios-cloud-upload\' : \'ion-ios-cloud-download\'\"></i>\r\n            <h2 ng-if=\"error.statusCode\">{{\'STATUS\' | translate}}: {{error.statusCode}}</h2>\r\n            <p ng-if=\"error.message\">{{error.message}}</p>\r\n            <p ng-if=\"!error.statusCode && !error.message\">{{\'UNKNOWN_ERROR\' | translate}}</p>\r\n        </div>\r\n    </div>\r\n</div>");}]);
//# sourceMappingURL=sync-subheader.js.map