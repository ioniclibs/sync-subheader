angular.module('starter.controllers', [])

    .controller('DashCtrl', function($scope, ErrorFactory) {

        $scope.loading = true;
        $scope.errorList = [];

        $scope.toggleLoading = function(){
            $scope.loading = !$scope.loading;
        };

        $scope.showError = function(){
            $scope.errorList = [];
            $scope.errorList.push(ErrorFactory.create(400, "Error ao obter dados do servidor", "get"));
            $scope.errorList.push(ErrorFactory.create(400, "Error ao enviar dados para o servidor", "put"));
        }
    })

    .controller('ChatsCtrl', function($scope, Chats) {
        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        $scope.chats = Chats.all();
        $scope.remove = function(chat) {
            Chats.remove(chat);
        }
    })

    .controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
        $scope.chat = Chats.get($stateParams.chatId);
    })

    .controller('AccountCtrl', function($scope) {
        $scope.settings = {
            enableFriends: true
        };
    });
